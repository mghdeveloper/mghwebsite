/**
 * Created by Castiel on 11/11/14.
 */
$(document).ready(function(){
    noty({
        "text":"Record updated successfully",
        "layout":"center",
        "type":"success", "textAlign":"center",
        "easing":"swing", "animateOpen":{"height":"toggle"},
        "animateClose":{"height":"toggle"},
        "speed":"500",
        "timeout":"5000",
        "closable":true,
        "closeOnSelfClick":true
    });
});