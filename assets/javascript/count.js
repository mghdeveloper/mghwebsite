/**
 * Created by Andrianina on 02/03/2018.
 */


$(document).ready(function() {
    $("#carouselExampleIndicators").on('slide.bs.carousel', function (ev) {


        var id = $(ev.relatedTarget).index();

        if(id==1){

            $('.count').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 3000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });

        }


    });

});

