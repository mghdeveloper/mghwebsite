<?php
    class Crv extends CI_Controller{
        function index(){
            $this->form_validation->set_rules('sujet', 'Sujet', 'required');
            if($this->form_validation->run()==FALSE){
            $this->load->view('templates/header');
            $this->load->view('crv');
            $this->load->view('templates/footer');}
            else{
                // $this->load->view('templates/header');
                $this->load->library('fpdf/fpdf');
                $this->load->library('email');
            
            $config ['upload_path'] = FCPATH . 'assets/uploads/files/';
            $config ['allowed_types'] = 'jpg|png|gif|docx|txt|pdf[xlsx' ;
            $config ['max_size'] = 3000 ; 
            $config ['max_width'] = 1920 ;
            $config ['max_height'] = 1080 ;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('image'))
            {
                    $error = array('error' => $this->upload->display_errors());
                    // print_r($error);
                    echo '<font face="Arial">Pas de fichier attach&eacute</font> <br/>';
            }
            else
            {
                    $data = $this->upload->data();
                    $imgPath = $data['full_path'];
            }

            $sujet =$this->input->post('sujet') ;
            $sujettype =$this->input->post('sujettype') ;
            $autres =$this->input->post('autres') ;	
            $date =$this->input->post('date') ;
            $escale =$this->input->post('escale') ;
            $nom =$this->input->post('nom') ;
            $service =$this->input->post('service') ;
            $email =$this->input->post('email') ;
            $anomalie =$this->input->post('anomalie') ;
            $consequence =$this->input->post('consequence') ;
            $proposition =$this->input->post('proposition') ;
            $reponse =$this->input->post('reponse') ;
            
            #$pdf = new $this->FPDF();
            $this->fpdf-> AddPage();
            $logo1 = 'assets/images/logo2.jpg';
            
            $this->fpdf->SetDrawColor(118,113,113);
            $this->fpdf-> SetFont("Arial","B",12);

            $this->fpdf->SetTextColor(28,102,152);
            $this->fpdf->Cell(50,36,$this->fpdf->Image($logo1, $this->fpdf->GetX(), $this->fpdf->GetY(), 50),1,0,'C');
            $this->fpdf->MultiCell(140,12,"COMPTE RENDU VOLONTAIRE \n Ed.02 Rev.00 MG.TR - Doc Ref.MSMS 3.3.3\nCRV du: ".date('Y-m-d'),1,'C',false);
            
            $this->fpdf->SetTextColor(0,0,0);
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(50,7,"Sujet",1,0,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> Cell(140,7,iconv('utf-8', 'cp1252', $sujet),1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(50,7,"Type sujet",1,0,'C');
            $this->fpdf->SetFont("Arial","",10);
            if($sujettype=="SECURITE"){
                $this->fpdf-> Cell(140,7,"SECURITE",1,1);}
            else{
                $this->fpdf-> Cell(140,7,iconv('utf-8', 'cp1252', $autres),1,1);}
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(50,7,"Date",1,0,'C');
            $this->fpdf->SetFont("Arial","",11);
            $this->fpdf-> Cell(140,7,$date,1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(50,7,"Escale",1,0,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> Cell(140,7,iconv('utf-8', 'cp1252', $escale),1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(50,7,"Nom",1,0,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> Cell(140,7,iconv('utf-8', 'cp1252', $nom),1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(50,7,"Service",1,0,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> Cell(140,7,$service,1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(50,7,"Email",1,0,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> Cell(140,7,$email,1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(190,7,"Anomalies",1,1,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> MultiCell(190,7,iconv('utf-8', 'cp1252', $anomalie),1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(190,7,iconv('utf-8', 'cp1252', "Conséquences"),1,1,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> MultiCell(190,7,iconv('utf-8', 'cp1252', $consequence),1,1);
            
            $this->fpdf->SetFont("Arial","B",11);
            $this->fpdf-> Cell(190,7,"Propositions",1,1,'C');
            $this->fpdf->SetFont("Arial","",10);
            $this->fpdf-> MultiCell(190,7,iconv('utf-8', 'cp1252', $proposition),1,1);

            //Fcpath -- chemin absolu 
            $pdfFilePath = FCPATH . "assets/pdfs/crv.pdf";
            $this->fpdf->Output($pdfFilePath, "F");
            
            if(!empty($email))
            $this->email->from($email);
            else
            $this->email->from('anonyme@mg-handling.com');

            $this->email->to('i.ramanana@mg-handling.com');
            $this->email->Bcc('l.rajaoarisoa@mg-handling.com');
            $this->email->subject('Compte rendu volontaire');
            $this->email->message('Attente de reponse:'.$reponse);
            
            if(!empty($imgPath))
            $this->email->attach($imgPath);

            $this->email->attach($pdfFilePath);
            if(!$this->email->send())
                echo 'Email non envoye/ Tsy lasa o';
            else
                echo '<font face="Arial"><p>Merci pour votre participation &agrave; notre S&eacute;curit&eacute; 
                <br/><em>Mankasitraka noho ny fandraisanao anjara mba ho fiarovana ny ASANTSIKA </em></p>
                Nouveau compte-rendu ? <em>Hanao tatitra vaovao?</em> <a href="http://www.mg-handling.com/crv">Cliquez ici - Tsindrio eto</a></font>';
            
            $ouverture= opendir(FCPATH . 'assets/pdfs/');
            unlink($pdfFilePath);
            closedir($ouverture);
            
        
            if (!empty($imgPath)){
                $ouverture= opendir(FCPATH . 'assets/uploads/files/');
                unlink($imgPath);
                closedir($ouverture);
            }
            
            }
        }
    }