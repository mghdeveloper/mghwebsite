<?php
/**
 * Created by PhpStorm.
 * User: Andrianina
 * Date: 19/12/2014
 * Time: 13:25
 */

class Maintenance extends CI_Controller {


    public function index(){

        $this->layout->set_theme('blank');
        $this->layout->view('maintenance');

    }

} 