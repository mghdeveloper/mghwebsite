<?php
/**
 * User: Lily Rajaoarisoa
 * Date: 28/02/18
 * Time: 08:18
 */

class Ourcustomers extends MY_Controller{

    public function index(){


        $data=array();

        $title = $this->lang->line('nav_customer');
        $this->layout->set_titre($title);
        $this->layout->views('frontend/shared/navigation');
        $this->layout->view('frontend/ourcustomers',$data);

    }


} 
