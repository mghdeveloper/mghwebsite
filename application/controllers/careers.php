<?php
/**
 * User: Lily Rajaoarisoa
 * Date: 28/02/18
 * Time: 08:19
 */

class Careers extends MY_Controller{

    public function index(){


        $data=array();

        $title = $this->lang->line('nav_careers');
        $this->layout->set_titre($title);
        $this->layout->views('frontend/shared/navigation');
        $this->layout->view('frontend/careers',$data);
    }


} 
