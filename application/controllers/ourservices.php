<?php
/**
 * User: Lily Rajaoarisoa
 * Date: 28/02/18
 * Time: 08:18
 */

class Ourservices extends MY_Controller{

    public function index(){


        $data=array();

        $title = $this->lang->line('nav_services');
        $this->layout->set_titre($title);
        $this->layout->views('frontend/shared/navigation');
        $this->layout->view('frontend/ourservices',$data);
		$this->layout->set_theme('default');
    }


} 
