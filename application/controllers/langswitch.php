<?php
/**
 * Created by PhpStorm.
 * User: Castiel
 * Date: 05/11/14
 * Time: 11:24
 */

class LangSwitch extends MY_Controller{

    function switchLanguage($language = "", $url="") {
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        //echo $url;
        redirect(base_url($url));
    }

} 