<?php
/**
 * Created by PhpStorm.
 * User: Castiel
 * Date: 09/08/14
 * Time: 01:37
 */

class Home extends MY_Controller{

    public function index(){


        $data=array();

        $title = $this->lang->line('nav_home');
        $this->layout->set_titre($title);
        $this->layout->ajouter_js('count');
        $this->layout->views('frontend/shared/navigation');
        $this->layout->view('frontend/index',$data);

    }


} 