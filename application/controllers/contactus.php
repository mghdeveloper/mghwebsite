<?php

/**
 * User: Lily Rajaoarisoa
 * Date: 28/02/18
 * Time: 08:19
 */
class Contactus extends MY_Controller {

    public function index() {

        $data = array();
        $this->load->library('email');

        $this->form_validation->set_rules("name", $this->lang->line("name"), "required");
        $this->form_validation->set_rules("company", $this->lang->line("company"));
        $this->form_validation->set_rules("contact", $this->lang->line("email"));
        $this->form_validation->set_rules("services", $this->lang->line("service"));
        $this->form_validation->set_rules("subject", $this->lang->line("subject"));
        
        if ($this->form_validation->run()) {
            
            $value = $this->input->post();
//            var_dump($value); die;
            $this->email->from($value['contact'], $value['company'] .' - '. $value['name']);
            switch ($value['services']) {
                case 'all':
                    $this->email->to('sales@mg-handling.com');

                    break;
                case 'ramp':
                    $this->email->to('sales@mg-handling.com');

                    break;
                case 'passanger':
                    $this->email->to('sales@mg-handling.com');

                    break;
                case 'crew':
                    $this->email->to('sales@mg-handling.com');

                    break;
                case 'meetandAssist':
                    $this->email->to('sales@mg-handling.com');

                    break;

                default:
                    $this->email->to('sales@mg-handling.com');
                    break;
            }
            

            $this->email->subject('[Site Web - mg-handling ] ' . $value['services']);
            $this->email->message($value['subject']);


            if (!$this->email->send()) {
                echo 'Failed <br>';

                // Loop through the debugger messages.
                echo $this->email->print_debugger();

                // Remove the debugger messages as they're not necessary for the next attempt.
                //$this->email->clear_debugger_messages();
            }
            else {
                echo 'Sent';
            }
        } else {
            echo validation_errors();
        }
        $title = $this->lang->line('nav_contactus');
        $this->layout->set_titre($title);
        $this->layout->views('frontend/shared/navigation');
        $this->layout->view('frontend/contactus', $data);
    }

}
