<?php
/**
 * User: Lily Rajaoarisoa
 * Date: 27/02/18
 * Time: 15:01
 */

class Aboutus extends MY_Controller{

    public function index(){


        $data=array();

        $title = $this->lang->line('nav_aboutus');
        $this->layout->set_titre($title);
        $this->layout->views('frontend/shared/navigation');
        $this->layout->view('frontend/aboutus',$data);

    }


} 
