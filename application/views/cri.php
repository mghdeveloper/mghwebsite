<!-- <div class="container">
	<div class="row">
		<div class = "col-md-3">
			<img src="assets/images/logo2.jpg" style="width:200px ;">
			<h6 style="text-align: center ;">
			<b>Ed.02 Rév.00 MG.TR - Doc R&eacute;f.MSMS 3.3.3 </b></p></h6>
			<p>Ce formulaire a été fait pour identifier les dangers/pour améliorer les services.<br/>
			<em>Natao ity mba hisorohana ny loza mety hitranga/mba ho fanatsaràna ny asa</em></p>
			<p><u>Dangers:</u> Situation, évènement ou circonstance succeptible d'engendrer un incident ou un accident. <br/>
			<b><u><em>Loza mety hitranga:<em></u> <em>Zava-misy, trangan-javatra na vanim-potoana mety hamono na handratra olona na hanimba fitaovana.</em></b></p>
			<p>Tous les comptes rendus, courriels et autres informations adressées au Service Réglementation et SMS seront traités 
				dans la plus stricte confidentialité et aucun tiers ne sera impliqué.<br/>
			<em>Ireo tatitra rehetra, mailaika, sy ireo fanampim-baovao hafa alefa any amin'ny Service Réglemenantation et SMS dia 
				raiketina sy alalinina ao anatin'ny tsiambaratelo tanteraka ary tsy hisy sampana hafa hampafantarina sy hizarana izany</em></p>
		</div>
		<div class = "col-md-8"> -->
			<h1>COMPTE-RENDU VOLONTAIRE</h1>
			<?php echo form_open_multipart('/crv'); ?>
				<table>
					<tr>
						<td class="tdtitre">
							<!-- <label for="sujet" class="form-control"><strong>Sujet</strong></label> -->
							Sujet
						</td>
						<td style="text-align:right">
							<input type="text" class="form-control" name="sujet" required>
						</td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td class="tdcenter">
							<div class="form-radio form-check-inline">
								<input type="radio" class="form-radio-input" name="sujettype" id="1" value="SECURITE" >
								<label for="1" class="form-radio-label"> SECURITE</label>
							</div>
						</td>
						<td class="tdcenter">
							<div class="form-radio form-check-inline">
								<input type="radio" class="form-radio-input" name="sujettype" id="2" value="AUTRES">
								<label for="2" class="form-radio-label"> AUTRES</label>
								<pre>  </pre><input type="text" name="autres" class="form-control">
							</div>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td class="tdtitre">Date</td>
						<td><input type="date" class="form-control" name="date" required></td>
					</tr>
					<tr>
						<td class="tdtitre">Escale concernée / <em>Toerana: </em></td>
						<td><input type="text" class="form-control" name="escale"></td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td colspan="3"><strong>Vos coordonnées / <em>Ny mombamomba anao (tsy voatery)</em></strong></td>
					</tr>
					<tr>
						<td class="tdtitre">Nom et prénoms: </td>
						<td class="tdtitre">Domaine:</td>
						<td class="tdtitre">Email :</td>
					</tr>
					<tr>
						<td><input type="text" name="nom" class="form-control"></td>
						<td>
							<select name="service" id="SelectService" class="form-control">
								<option value="all">All</option>
								<option value="ramp">Ramp Services</option>
								<option value="passenger">Passenger Services</option>
								<option value="crew">Crew Services</option>
								<option value="meetandassist">Meet-and-Assist</option>
								<option value="other">Other Services</option>
							</select>
						</td>
						<td><input type="email" name="email" class="form-control"></td>
					</tr>
				</table>
				<br/>
				<table>
					<tr>
						<td class="tdtitre">
							Anomalies constatées <em>(Ny zavatra tsy mety izay tsikaritra/Fitantarana ny zava-nitranga/zava-misy)</em>
						</td>
						<td>
							<textarea class="form-control" id="anomalie" name="anomalie" rows="3" required></textarea>
						</td>
					</tr>
					<tr>
						<td class="tdtitre">
							Conséquence probable <em>(Ny mety ho vokatr'izany raha ny hevitrao)</em>
						</td>
						<td><textarea class="form-control" id="consequence" name="consequence" rows="2" ></textarea></td>
					</tr>
					<tr>
						<td class="tdtitre">
							Votre proposition <em>(Ny soso-kevitrao ho fisorohana na fanitsiana arosonao, raha manana ianao)</em>
						</td>
						<td><textarea class="form-control" id="proposition" name="proposition" rows="3" ></textarea></td>
					</tr>
					<tr><td class="tdtitre">Pièces jointes (max: 3Mo):</td><td><input type="file" name="image"></td></tr>
				</table>
				<br/>
				<table>
					<tr>
						<td style="font-size:10.5pt; width:30%">Désirez-vous recevoir une réponse ?</td>
						<td>
							<input class="form-radio-input" type="radio" name="reponse" id="3" value="oui" radioed>
							<label class="form-radio-label" for="3">Oui</label>
						</td>
						<td>
							<input class="form-radio-input" type="radio" name="reponse" id="4" value="non">
							<label class="form-radio-label" for="4">Non</label>
						</td>
						<td style="text-right"><input class="btn btn-primary" type="submit" value="Valider" name="submit"></td>
				</table>
			<?php echo form_close(); ?>
		<!-- </div>
	</div>
</div> -->