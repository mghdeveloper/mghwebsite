<br />
<div class="container">
    <div class="row">
        <blockquote class="col-12 text-center">
            <h1> <?php echo $this->lang->line('sv_page_title');?> </h1>
            <footer class="blockquote-footer"><?php echo $this->lang->line('service_pride');?></footer>
            <br />
            <p><?php echo $this->lang->line('service_listing');?></p>
        </blockquote>
    </div>
    <div class="row">


        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_ramp_title');?></h3>
            <?php echo $this->lang->line('sv_ramp_content');?>
        </div>
        <div class="col col-5">
            <img src="assets/images/marshalling.jpg" class="img-thumbnail img-fluid" >
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col col-5">
            <img src="assets/images/passengers.jpg" class="img-thumbnail img-fluid" >
        </div>
        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-warning txt-s2"><?php echo $this->lang->line('sv_psr_title');?></h3>
            <?php echo $this->lang->line('sv_psr_content');?>
        </div>
    </div>
    <br />
    <div class="row">

        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_baggage_title');?></h3>
            <?php echo $this->lang->line('sv_baggage_content');?>
            
        </div>
        <div class="col col-5">
            <img src="assets/images/baguage.jpg" class="img-thumbnail img-fluid" >
        </div>


    </div>

    <br />



    <div class="row">

        <div class="col col-5">
            <img src="assets/images/cargo.jpg" class="img-thumbnail img-fluid" >
        </div>
        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-warning txt-s2"><?php echo $this->lang->line('sv_cargo_title');?></h3>
            <?php echo $this->lang->line('sv_cargo_content');?>
        </div>

    </div>


    <br />


    <div class="row">

        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_lounge_title');?></h3>
            <?php echo $this->lang->line('sv_lounge_content');?>

        </div>
        <div class="col col-5">
            <img src="assets/images/lounge.JPG" class="img-thumbnail img-fluid" >
        </div>


    </div>


    <br />



    <div class="row">



        <div class="col col-5">
            <img src="assets/images/assist.jpg" class="img-thumbnail img-fluid" >
        </div>

        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-warning txt-s2"><?php echo $this->lang->line('sv_assist_title');?></h3>
            <?php echo $this->lang->line('sv_assist_content');?>
        </div>

    </div>


    <br />


    <div class="row">
        <div class="col col-12 text-center card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_customize_title');?></h3>
            <p><?php echo $this->lang->line('sv_customize_content');?></p>
        </div>
    </div>

    <br />


</div>




