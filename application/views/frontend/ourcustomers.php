<div class="container">
    </br>
    <div align="center">
        <h1>Our Valued Customers</h1>
    </div>

    <div>
        <blockquote class="blockquote text-center"><?php echo $this->lang->line('content_head_custom');?></blockquote>
    </div>
    <div class="midalign">
        <div class="row">


        <!-- SECTION REGULAR BASIS FLIGHT -->
        <div class="col-6">
        <div class="card card-body bg-light-grey">
        <div class="row">
            <div class="col text-center">
                <h3 class="badge badge-success txt-s4"><?php echo $this->lang->line('regular');?></h3>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col"><img src="assets/images/logo/uu.png" alt="Air Austral" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/af.png" alt="Air France" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/md.png" alt="Air Madagascar" class="img-thumbnail img-fluid"></div>
        </div>
        <br/>
        <div class="row">
            <div class="col"><img src="assets/images/logo/hm.png" alt="Air Seychelles" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/sa.png" alt="South African Airlink" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/kq.png" alt="Kenya Airways" class="img-thumbnail img-fluid"></div>
        </div>
        <br />
        <div class="row">
            <div class="col"><img src="assets/images/logo/et.png" alt="Ethiopian Airlines" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/ss.png" alt="Corsair" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/tk.png" alt="Turkish Airlines" class="img-thumbnail img-fluid"></div>
        </div>
        </br>
        <div class="row">
            <!--<div class="col"></div>-->  
            <div class="col"><img src="assets/images/logo/mk.png" alt="Air Mauritius" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/ewa.png" alt="Ewa Air" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/tz.JPG" alt="Tsaradia" class="img-thumbnail img-fluid"></div>
            <!--<div class="col"></div>-->
        </div>
        </br>
        </div>
        </div>
<!--
        <br />
        <div class="row">
            <div class="col"></div>
        </div>
        </div>
        </div>
-->
        <br />
        <br />


        <!-- SECTION ADHOC FLIGHT-->
        <div class="col-6">
        <div class="card card-body bg-light-grey">
        <div class="row">
            <div class="col text-center">
                <h3 class="badge badge-warning txt-s4"><?php echo $this->lang->line('adhoc');?></h3>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col"><img src="assets/images/logo/aircargoglobal.png" alt="Air Cargo Chartering" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/cjs.png" alt="Continental Jet Services" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/ufs.png" alt="United Flight Services" class="img-thumbnail img-fluid"></div>
            <div class="col"><img src="assets/images/logo/airindia.png" alt="Air India" class="img-thumbnail img-fluid"></div>
        </div>

        <br />
        <div class="row">
            <div class="col"><img src="assets/images/logo/aurora.png" alt="Aurora Aviation" width=100></div>
            <div class="col"><img src="assets/images/logo/wfs.png" alt="World Fuel Services" width=100></div>
            <div class="col"><img src="assets/images/logo/palm.png" alt="Palm Aviation" width=100></div>
            <div class="col"><img src="assets/images/logo/tss.png" alt="Trip Support Services" width=100></div>
        </div>

        <br />
        <div class="row">
            <div class="col"><img src="assets/images/logo/yulounge.png" alt="YU Lounge" width=100></div>
            <div class="col"><img src="assets/images/logo/jetex.png" alt="JETEX" width=100></div>
            <div class="col"><img src="assets/images/logo/ram.png" alt="Royal Air Maroc" width=100></div>
            <div class="col"><img src="assets/images/logo/flytag.png" alt="Fly Tag" width=100></div>
        </div>
        </div>
        </div>
        </div>

    </div>

</div>

<br />
<br />
