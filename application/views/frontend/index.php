<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('assets/images/ramp.png')">

                <div>
                    <div class="row padding-slider-0">
                        <div class="col offset-2 col-6">
                            <h3 class="txt-x4 badge"><?php echo $this->lang->line('society_name');?></h3>
                        </div>
                        <div class="col col-4"></div>
                    </div>

                    <div class="row">
                        <div class="col offset-2 col-6">
                            <p class="txt-x0 badge slide-title"><?php echo $this->lang->line('society_slogan');?></p>
                        </div>
                        <div class="col col-2"></div>
                    </div>
                    <div class="row">
                        <div class="col offset-2 col-3">

                            <blockquote class="blockquote card text-center" style="padding: 10px">
                                <p class="small">
                                    <?php echo $this->lang->line('slide1_text');?>
                                </p>
                                <!--

                                <footer class="blockquote-footer"><?php echo $this->lang->line('society_rh');?></footer>
                                -->
                            </blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col offset-3 col-4">
                            <a href="aboutus" class="btn btn btn-success"><?php echo $this->lang->line('society_aboutus');?><i class="fa fa-hand-o-right"></i></a>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col offset-5 col-2">
                            <img src="assets/images/agent.JPG" class="img-responsive img-thumbnail" alt="Responsive image">
                        </div>
                        <div class="col-2">
                            <img src="assets/images/coobo.jpg" class="img-responsive img-thumbnail" alt="Responsive image">
                        </div>
                        <div class="col-2">
                            <img src="assets/images/fret.jpg" class="img-responsive img-thumbnail" alt="Responsive image">
                        </div>
                    </div>

                </div>

            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('assets/images/marshalling.jpg')">


                <div>
                    <div class="row padding-slider-0">
                        <div class="col offset-2 col-6">
                            <h3 class="text-info txt-x4"><?php echo $this->lang->line('contenu_service');?></h3>
                            <div class="col col-5 card card-block">
                                <br />
                                <?php echo $this->lang->line('service_content');?>
                            </div>

                        </div>
                        <div class="col col-4">
                            <br />
                            <br />
                            <br />
                            <br />
                            <p class="text-info txt-x5"> <?php echo $this->lang->line('flt');?></p>
                            <p class="text-info txt-x5"> <?php echo $this->lang->line('psr');?></p>
                            <p class="text-info txt-x5"><?php echo $this->lang->line('cgo');?></p>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col offset-2 col-6">
                            <a href="ourservices" class="btn btn btn-warning"><?php echo $this->lang->line('offer');?></a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('assets/images/cargo.png')">

                <div class="container">


                    <div class="row padding-slider-0">
                        <div class="col col-12 text-center">
                            <h3 class="txt-x3 badge"><?php echo $this->lang->line('customer_value_content');?></h3>
                        </div>


                    </div>



                    <div class="row">



                        <div class="offset-1 col-4">
                                <div class="row">
                                    <div class="col">
                                        <img src="assets/images/slide3_1.jpg" alt="image 1" class="img-responsive img-thumbnail">
                                    </div>
                                 </div>

                                <br />

                                <div class="row" >
                                    <div class="col">
                                         <img src="assets/images/slide3-2.JPG" alt="image 2" class="img-responsive img-thumbnail">

                                    </div>
                                </div>

                         </div>


                        <div class="col-6">

                            <blockquote class="blockquote card text-center" style="padding :10px">
                                <p class="small">
                                    <?php echo $this->lang->line('slide3-text');?>
                                </p>
                            </blockquote>
                            <div class="col offset-4 col-6">
                                <a href="ourcustomers" class="btn btn-lg btn-danger"><?php echo $this->lang->line('button-customers');?></a>
                            </div>
                        </div>

                    </div>

                </div>


                <!--
                <div class="carousel-caption d-none d-md-block">
                    <h3><?php echo $this->lang->line('valued_customer_title');?></h3>
                    <?php echo $this->lang->line('valued_customer_content');?>
                </div>
                -->
            </div>
            <!-- Slide Four - Set the background image for this slide in the line below
            <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">-->
            <div class="carousel-item" style="background-image: url('assets/images/meet.jpg')">

                <div class="container">

                    <div class="row padding-slider-0">
                        <div class="col col-12 text-center">
                            <h3 class="txt-x3 badge slide4-title"><?php echo $this->lang->line('contenu_title_well_home');?></h3>
                        </div>

                    </div>

                    <br />
                    <div class="row">
                        
                        <br />
                        <div class="offset-1 card card-block col-12">

                            <div class="row">
                            <br />
                            <br />
                                <div class="col col-8 text-center">
                                    <?php echo $this->lang->line('contenu_well_home');?>
                                </div>
                                <div class="col col-4">
                                     <img src="assets/images/ISAGO.jpg" alt="Certification ISAGO" class="img-responsive img-thumbnail">
                                </div>
                                <br />
                                <br />
                            </div>

                          
                        </div>
                        <!--

                        <div class="offset-8 col-4">
                            <div class="row">

                                <img src="assets/images/slide4-1.jpg" alt="image 1" class="img-responsive img-thumbnail">


                            </div>
                            </br>
                            <div class="row">

                                <img src="assets/images/slide4-2.JPG" alt="image 1" class="img-responsive img-thumbnail">


                            </div>

                        </div>
                        -->
                    </div>

                    <div class="row">
                        <div class="col offset-2 col-5">
                            </br>
                            <a href="contactus" class="btn btn-lg btn-warning"><?php echo $this->lang->line('contenu_bouton_well_home');?></a>
                        </div>
                    </div>


                </div>

                <!--
                <div class="carousel-caption d-none d-md-block">
                    <h3><?php echo $this->lang->line('valued_customer_content');?></h3>
                    <p><?php echo $this->lang->line('contact_value');?></p>
                </div>
                -->

            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only"><?php echo $this->lang->line('previous');?></span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only"><?php echo $this->lang->line('next');?></span>
        </a>
    </div>
</header>

<!-- Page Content
<section class="py-5">
    <div class="container">
        <h1>Half Slider by Start Bootstrap</h1>
        <p>The background images for the slider are set directly in the HTML using inline CSS. The rest of the styles for this template are contained within the <code>half-slider.css</code> file.</p>
    </div>
</section>
-->

<br />
<div class="container">


	<!-- Section History -->
	<section id="history">
		<div align="center">
			<h1><?php echo $this->lang->line('title_history');?></h1>
		</div>
		<div>
			<p><?php echo $this->lang->line('content_history');?></p>
		</div>
	</section>
	<!-- End Section History -->

	<!-- Section Pride -->
	<section id="pride">
		<h2 class="txt-s4"><?php echo $this->lang->line('title_pride');?></h2>
		<?php echo $this->lang->line('content_pride');?>
	</section>
	<!-- End Section Pride -->

	<!-- Section Team -->
	<section id="team">
		<h2 class="txt-s4"><?php echo $this->lang->line('title_team');?></h2>
		<br />
		<br />
		<div class="row">
			<div class="col-lg-3 text-center">
				<img src="assets/images/persona_thumbnail.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Noro RAZAFIMAHEFA</strong>
				<p class="small"><em><?php echo $this->lang->line('job_ceo');?></em></p>

			</div>

			<div class="col-lg-3 text-center">
				<img src="assets/images/persona_thumbnail.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Volasoa RANARISON</strong>
				<p class="small"><em><?php echo $this->lang->line('job_op');?></em></p>
			</div>

			<div class="col-lg-3 text-center">
				<img src="assets/images/persona_thumbnail.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Alisoa RAKOTO</strong>
				<p class="small"><em><?php echo $this->lang->line('job_afd');?></em></p>
			</div>

			<div class="col-lg-3 text-center">
				<img src="assets/images/persona.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Njaka NASOLONIAINA</strong>
				<p class="small"><em><?php echo $this->lang->line('job_lid');?></em></p>
			</div>


		</div>


	</section>
	<!-- End Section Team -->

	<!-- Section Booklet -->
	<!--
		
	<section id="booklet">
		<h2 class="txt-s4"><?php echo $this->lang->line('title_booklet');?></h2>
		<p><?php echo $this->lang->line('content_booklet');?></p>
		<p><a href="#" target="_blank"><?php echo $this->lang->line('booklet-link');?></a> </p>
	</section>
	-->
	<!-- End Section Booklet -->
    <div class="row">
        <blockquote class="col-12 text-center">
            <h1> <?php echo $this->lang->line('sv_page_title');?> </h1>
            <footer class="blockquote-footer"><?php echo $this->lang->line('service_pride');?></footer>
            <br />
            <p><?php echo $this->lang->line('service_listing');?></p>
        </blockquote>
    </div>
    <div class="row">


        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_ramp_title');?></h3>
            <?php echo $this->lang->line('sv_ramp_content');?>
        </div>
        <div class="col col-5">
            <img src="assets/images/marshalling.jpg" class="img-thumbnail img-fluid" >
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col col-5">
            <img src="assets/images/passengers.jpg" class="img-thumbnail img-fluid" >
        </div>
        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-warning txt-s2"><?php echo $this->lang->line('sv_psr_title');?></h3>
            <?php echo $this->lang->line('sv_psr_content');?>
        </div>
    </div>
    <br />
    <div class="row">

        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_baggage_title');?></h3>
            <?php echo $this->lang->line('sv_baggage_content');?>
            
        </div>
        <div class="col col-5">
            <img src="assets/images/baguage.jpg" class="img-thumbnail img-fluid" >
        </div>


    </div>

    <br />



    <div class="row">

        <div class="col col-5">
            <img src="assets/images/cargo.jpg" class="img-thumbnail img-fluid" >
        </div>
        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-warning txt-s2"><?php echo $this->lang->line('sv_cargo_title');?></h3>
            <?php echo $this->lang->line('sv_cargo_content');?>
        </div>

    </div>


    <br />


    <div class="row">

        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_lounge_title');?></h3>
            <?php echo $this->lang->line('sv_lounge_content');?>

        </div>
        <div class="col col-5">
            <img src="assets/images/lounge.JPG" class="img-thumbnail img-fluid" >
        </div>


    </div>


    <br />



    <div class="row">



        <div class="col col-5">
            <img src="assets/images/assist.jpg" class="img-thumbnail img-fluid" >
        </div>

        <div class="col col-7 card">
            <br />
            <h3 class="badge badge-warning txt-s2"><?php echo $this->lang->line('sv_assist_title');?></h3>
            <?php echo $this->lang->line('sv_assist_content');?>
        </div>

    </div>


    <br />


    <div class="row">
        <div class="col col-12 text-center card">
            <br />
            <h3 class="badge badge-info txt-s2"><?php echo $this->lang->line('sv_customize_title');?></h3>
            <p><?php echo $this->lang->line('sv_customize_content');?></p>
        </div>
    </div>

    


    <div align="center">
        <h1>Our Valued Customers</h1>
    </div>

    <div>
        <blockquote class="blockquote text-center"><?php echo $this->lang->line('content_head_custom');?></blockquote>
    </div>
    <div class="midalign">
        <div class="row">


            <!-- SECTION REGULAR BASIS FLIGHT -->
            <div class="col-6">
                <div class="card card-body bg-light-grey">
                    <div class="row">
                        <div class="col text-center">
                            <h3 class="badge badge-success txt-s4"><?php echo $this->lang->line('regular');?></h3>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col"><img src="assets/images/logo/uu.png" alt="Air Austral" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/af.png" alt="Air France" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/md.png" alt="Air Madagascar" class="img-thumbnail img-fluid"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col"><img src="assets/images/logo/hm.png" alt="Air Seychelles" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/sa.png" alt="South African Airlink" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/kq.png" alt="Kenya Airways" class="img-thumbnail img-fluid"></div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col"><img src="assets/images/logo/et.png" alt="Ethiopian Airlines" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/ss.png" alt="Corsair" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/tk.png" alt="Turkish Airlines" class="img-thumbnail img-fluid"></div>
                    </div>
                    </br>
                    <div class="row">
                        <!--<div class="col"></div>-->  
                        <div class="col"><img src="assets/images/logo/mk.png" alt="Air Mauritius" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/ewa.png" alt="Ewa Air" class="img-thumbnail img-fluid"></div>
                        <div class="col"><img src="assets/images/logo/tz.JPG" alt="Tsaradia" class="img-thumbnail img-fluid"></div>
                        <!--<div class="col"></div>-->
                    </div>
                    </br>
                </div>
            </div>
            <!-- SECTION REGULAR BASIS FLIGHT END -->
        <br />
        <!--<div class="row">
            <div class="col"></div>
        </div>
        </div>
        </div>
        -->


        <!-- SECTION ADHOC FLIGHT-->
        <div class="col-6">
            <div class="card card-body bg-light-grey">
                <div class="row">
                    <div class="col text-center">
                        <h3 class="badge badge-warning txt-s4"><?php echo $this->lang->line('adhoc');?></h3>
                    </div>
                </div>

                <br />

                <div class="row">
                    <div class="col"><img src="assets/images/logo/aircargoglobal.png" alt="Air Cargo Chartering" class="img-thumbnail img-fluid"></div>
                    <div class="col"><img src="assets/images/logo/cjs.png" alt="Continental Jet Services" class="img-thumbnail img-fluid"></div>
                    <div class="col"><img src="assets/images/logo/ufs.png" alt="United Flight Services" class="img-thumbnail img-fluid"></div>
                    <div class="col"><img src="assets/images/logo/airindia.png" alt="Air India" class="img-thumbnail img-fluid"></div>
                </div>

                <br />
                <div class="row">
                    <div class="col"><img src="assets/images/logo/aurora.png" alt="Aurora Aviation" width=100></div>
                    <div class="col"><img src="assets/images/logo/wfs.png" alt="World Fuel Services" width=100></div>
                    <div class="col"><img src="assets/images/logo/palm.png" alt="Palm Aviation" width=100></div>
                    <div class="col"><img src="assets/images/logo/tss.png" alt="Trip Support Services" width=100></div>
                </div>

                <br />
                <div class="row">
                    <div class="col"><img src="assets/images/logo/yulounge.png" alt="YU Lounge" width=100></div>
                    <div class="col"><img src="assets/images/logo/jetex.png" alt="JETEX" width=100></div>
                    <div class="col"><img src="assets/images/logo/ram.png" alt="Royal Air Maroc" width=100></div>
                    <div class="col"><img src="assets/images/logo/flytag.png" alt="Fly Tag" width=100></div>
                </div>
            </div>
        </div>
        <!-- SECTION ADHOC FLIGHT END-->
    </div>

    <br />

    <div align="center">
        <h1><?php echo $this->lang->line("career_title");?></h1>
    </div>



    <div class="row">
        <div class="col col-8">
                <?php echo $this->lang->line("career_content");?>
        </div>
        <div class="col col-4">
            <img src="assets/images/recruitment.jpg" class="img-responsive img-thumbnail">

            <br />
            <br />

            <div class="card bg-light-grey">
                </br>
                <form class="card-body">
                    <div class="form-group">
                        <label for="InputFirstName"><strong><?php echo $this->lang->line('first_name');?></strong></label>
                        <input type="name" class="form-control" id="InputFirstName" placeholder="<?php echo $this->lang->line('input_first_name');?>" required>
                    </div>
                    <div class="form-group">
                        <label for="InputLastName"><strong><?php echo $this->lang->line('last_name');?></strong></label>
                        <input type="name" class="form-control" id="InputLastName" placeholder="<?php echo $this->lang->line('input_last_name');?>" required>
                    </div>
                    <div class="form-group">
                        <label for="InputEmail"><strong><?php echo $this->lang->line('email');?></strong></label>
                        <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="<?php echo $this->lang->line('input_email');?>" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="UploadCV"><strong><?php echo $this->lang->line('CV');?></strong></label>
                        <input type="file" class="form-control" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword, application/pdf" required>
                    </div>
                    <div class="form-group">
                        <label for="UploadLetter"><strong><?php echo $this->lang->line('application_letter');?></strong></label>
                        <input type="file" class="form-control" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword, application/pdf" required>
                    </div>

                    </br>

                    <div class="row">
                        <div class="col"></div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-block" action="mailto:contact@mg-handling.com?Subject=From website"><?php echo $this->lang->line('submit');?></button>
                        </div>
                        <div class="col"></div>

                    </div>
                </form>
            </div>



        </div>
    </div>




</div>



</div>





