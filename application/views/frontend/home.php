<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('assets/images/ramp.png')">

                <div>
                    <div class="row padding-slider-0">
                        <div class="col offset-2 col-6">
                            <h3 class="txt-x4 badge"><?php echo $this->lang->line('society_name');?></h3>
                        </div>
                        <div class="col col-4"></div>
                    </div>

                    <div class="row">
                        <div class="col offset-2 col-6">
                            <p class="txt-x0 badge slide-title"><?php echo $this->lang->line('society_slogan');?></p>
                        </div>
                        <div class="col col-2"></div>
                    </div>
                    <div class="row">
                        <div class="col offset-2 col-3">

                            <blockquote class="blockquote card text-center" style="padding: 10px">
                                <p class="small">
                                    <?php echo $this->lang->line('slide1_text');?>
                                </p>
                                <!--

                                <footer class="blockquote-footer"><?php echo $this->lang->line('society_rh');?></footer>
                                -->
                            </blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col offset-3 col-4">
                            <a href="aboutus" class="btn btn btn-success"><?php echo $this->lang->line('society_aboutus');?><i class="fa fa-hand-o-right"></i></a>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col offset-5 col-2">
                            <img src="assets/images/agent.JPG" class="img-responsive img-thumbnail" alt="Responsive image">
                        </div>
                        <div class="col-2">
                            <img src="assets/images/coobo.jpg" class="img-responsive img-thumbnail" alt="Responsive image">
                        </div>
                        <div class="col-2">
                            <img src="assets/images/fret.jpg" class="img-responsive img-thumbnail" alt="Responsive image">
                        </div>
                    </div>

                </div>

            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('assets/images/marshalling.jpg')">


                <div>
                    <div class="row padding-slider-0">
                        <div class="col offset-2 col-6">
                            <h3 class="text-info txt-x4"><?php echo $this->lang->line('contenu_service');?></h3>
                            <div class="col col-5 card card-block">
                                <br />
                                <?php echo $this->lang->line('service_content');?>
                            </div>

                        </div>
                        <div class="col col-4">
                            <br />
                            <br />
                            <br />
                            <br />
                            <p class="text-info txt-x5"> <?php echo $this->lang->line('flt');?></p>
                            <p class="text-info txt-x5"> <?php echo $this->lang->line('psr');?></p>
                            <p class="text-info txt-x5"><?php echo $this->lang->line('cgo');?></p>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col offset-2 col-6">
                            <a href="ourservices" class="btn btn btn-warning"><?php echo $this->lang->line('offer');?></a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('assets/images/cargo.png')">

                <div class="container">


                    <div class="row padding-slider-0">
                        <div class="col col-12 text-center">
                            <h3 class="txt-x3 badge"><?php echo $this->lang->line('customer_value_content');?></h3>
                        </div>


                    </div>



                    <div class="row">



                        <div class="offset-1 col-4">
                                <div class="row">
                                    <div class="col">
                                        <img src="assets/images/slide3_1.jpg" alt="image 1" class="img-responsive img-thumbnail">
                                    </div>
                                 </div>

                                <br />

                                <div class="row" >
                                    <div class="col">
                                         <img src="assets/images/slide3-2.JPG" alt="image 2" class="img-responsive img-thumbnail">

                                    </div>
                                </div>

                         </div>


                        <div class="col-6">

                            <blockquote class="blockquote card text-center" style="padding :10px">
                                <p class="small">
                                    <?php echo $this->lang->line('slide3-text');?>
                                </p>
                            </blockquote>
                            <div class="col offset-4 col-6">
                                <a href="ourcustomers" class="btn btn-lg btn-danger"><?php echo $this->lang->line('button-customers');?></a>
                            </div>
                        </div>

                    </div>

                </div>


                <!--
                <div class="carousel-caption d-none d-md-block">
                    <h3><?php echo $this->lang->line('valued_customer_title');?></h3>
                    <?php echo $this->lang->line('valued_customer_content');?>
                </div>
                -->
            </div>
            <!-- Slide Four - Set the background image for this slide in the line below
            <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">-->
            <div class="carousel-item" style="background-image: url('assets/images/meet.jpg')">

                <div class="container">

                    <div class="row padding-slider-0">
                        <div class="col col-12 text-center">
                            <h3 class="txt-x3 badge slide4-title"><?php echo $this->lang->line('contenu_title_well_home');?></h3>
                        </div>

                    </div>

                    <br />
                    <div class="row">
                        
                        <br />
                        <div class="offset-1 card card-block col-12">

                            <div class="row">
                            <br />
                            <br />
                                <div class="col col-8 text-center">
                                    <?php echo $this->lang->line('contenu_well_home');?>
                                </div>
                                <div class="col col-4">
                                     <img src="assets/images/ISAGO.jpg" alt="Certification ISAGO" class="img-responsive img-thumbnail">
                                </div>
                                <br />
                                <br />
                            </div>

                          
                        </div>
                        <!--

                        <div class="offset-8 col-4">
                            <div class="row">

                                <img src="assets/images/slide4-1.jpg" alt="image 1" class="img-responsive img-thumbnail">


                            </div>
                            </br>
                            <div class="row">

                                <img src="assets/images/slide4-2.JPG" alt="image 1" class="img-responsive img-thumbnail">


                            </div>

                        </div>
                        -->
                    </div>

                    <div class="row">
                        <div class="col offset-2 col-5">
                            </br>
                            <a href="contactus" class="btn btn-lg btn-warning"><?php echo $this->lang->line('contenu_bouton_well_home');?></a>
                        </div>
                    </div>


                </div>

                <!--
                <div class="carousel-caption d-none d-md-block">
                    <h3><?php echo $this->lang->line('valued_customer_content');?></h3>
                    <p><?php echo $this->lang->line('contact_value');?></p>
                </div>
                -->

            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only"><?php echo $this->lang->line('previous');?></span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only"><?php echo $this->lang->line('next');?></span>
        </a>
    </div>
</header>

<!-- Page Content
<section class="py-5">
    <div class="container">
        <h1>Half Slider by Start Bootstrap</h1>
        <p>The background images for the slider are set directly in the HTML using inline CSS. The rest of the styles for this template are contained within the <code>half-slider.css</code> file.</p>
    </div>
</section>
-->
