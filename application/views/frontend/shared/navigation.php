
<!-- Navigation -->
<nav class="navbar navbar-expand-lg nav- bg-light sticky-top">
    <div class="container">
        <a class="navbar-brand logo" href="home"><img src="assets/images/logo/mini-mgh.jpg" class="img-thumbnail"  alt="MGH"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <li class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <!--<li class="nav-item">
                     <a class="nav-link" href="home"><?php echo $this->lang->line('nav_home'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="aboutus"><?php echo $this->lang->line('nav_aboutus'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ourservices"><?php echo $this->lang->line('nav_services'); ?></a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" href="ourcustomers"><?php echo $this->lang->line('nav_customer'); ?></a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" href="careers"><?php echo $this->lang->line('nav_careers'); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contactus"><?php echo $this->lang->line('nav_contactus'); ?></a>
                </li>-->
                <li class="nav-item">
                    <div class="nav-link bg-light">

                        <a href="<?php echo site_url("langswitch/switchLanguage/english")."/". utf8_encode(uri_string()); ?>"><img src="<?php echo img_url('flags/gb.png'); ?>"><span> ENG</span></a>
                        <span> | </span>

                        <a href="<?php echo site_url("langswitch/switchLanguage/french")."/". utf8_encode(uri_string()); ?>"><img src="<?php echo img_url('flags/fr.png'); ?>" /><span> FR</span></a>

                    </div>
                </li>
            </ul>
        </div>

    </div>
</nav>
