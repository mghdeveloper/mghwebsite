<br xmlns="http://www.w3.org/1999/html"/>

<!-- Begin Container -->
<div class="container">

	<!-- Section History -->
	<section id="history">
		<div align="center">
			<h1><?php echo $this->lang->line('title_history');?></h1>
		</div>
		<div>
			<p><?php echo $this->lang->line('content_history');?></p>
		</div>
	</section>
	<!-- End Section History -->

	<!-- Section Pride -->
	<section id="pride">
		<h2 class="txt-s4"><?php echo $this->lang->line('title_pride');?></h2>
		<?php echo $this->lang->line('content_pride');?>
	</section>
	<!-- End Section Pride -->

	<!-- Section Team -->
	<section id="team">
		<h2 class="txt-s4"><?php echo $this->lang->line('title_team');?></h2>
		<br />
		<br />
		<div class="row">
			<div class="col-lg-3 text-center">
				<img src="assets/images/persona_thumbnail.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Noro RAZAFIMAHEFA</strong>
				<p class="small"><em><?php echo $this->lang->line('job_ceo');?></em></p>

			</div>

			<div class="col-lg-3 text-center">
				<img src="assets/images/persona_thumbnail.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Volasoa RANARISON</strong>
				<p class="small"><em><?php echo $this->lang->line('job_op');?></em></p>
			</div>

			<div class="col-lg-3 text-center">
				<img src="assets/images/persona_thumbnail.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Alisoa RAKOTO</strong>
				<p class="small"><em><?php echo $this->lang->line('job_afd');?></em></p>
			</div>

			<div class="col-lg-3 text-center">
				<img src="assets/images/persona.jpg" class="img-thumbnail img-responsive" />
				<br />
				<strong>Njaka NASOLONIAINA</strong>
				<p class="small"><em><?php echo $this->lang->line('job_lid');?></em></p>
			</div>


		</div>


	</section>
	<!-- End Section Team -->

	<!-- Section Booklet -->
	<!--
		
	<section id="booklet">
		<h2 class="txt-s4"><?php echo $this->lang->line('title_booklet');?></h2>
		<p><?php echo $this->lang->line('content_booklet');?></p>
		<p><a href="#" target="_blank"><?php echo $this->lang->line('booklet-link');?></a> </p>
	</section>
	-->
	<!-- End Section Booklet -->


</div>
<!-- End Container -->