<br/>
<div class="container">
    <div align="center">
        <h1><?php echo $this->lang->line("career_title");?></h1>
    </div>



    <div class="row">
        <div class="col col-8">
                <?php echo $this->lang->line("career_content");?>
        </div>
        <div class="col col-4">
            <img src="assets/images/recruitment.jpg" class="img-responsive img-thumbnail">

            <br />
            <br />

            <div class="card bg-light-grey">
                </br>
                <form class="card-body">
                    <div class="form-group">
                        <label for="InputFirstName"><strong><?php echo $this->lang->line('first_name');?></strong></label>
                        <input type="name" class="form-control" id="InputFirstName" placeholder="<?php echo $this->lang->line('input_first_name');?>" required>
                    </div>
                    <div class="form-group">
                        <label for="InputLastName"><strong><?php echo $this->lang->line('last_name');?></strong></label>
                        <input type="name" class="form-control" id="InputLastName" placeholder="<?php echo $this->lang->line('input_last_name');?>" required>
                    </div>
                    <div class="form-group">
                        <label for="InputEmail"><strong><?php echo $this->lang->line('email');?></strong></label>
                        <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="<?php echo $this->lang->line('input_email');?>" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="UploadCV"><strong><?php echo $this->lang->line('CV');?></strong></label>
                        <input type="file" class="form-control" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword, application/pdf" required>
                    </div>
                    <div class="form-group">
                        <label for="UploadLetter"><strong><?php echo $this->lang->line('application_letter');?></strong></label>
                        <input type="file" class="form-control" accept="application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword, application/pdf" required>
                    </div>

                    </br>

                    <div class="row">
                        <div class="col"></div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-block" action="mailto:contact@mg-handling.com?Subject=From website"><?php echo $this->lang->line('submit');?></button>
                        </div>
                        <div class="col"></div>

                    </div>
                </form>
            </div>



        </div>
    </div>


</div>
<br/>
<br/>

