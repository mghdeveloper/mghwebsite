<div class="container">
    <br/>

    <div align="center"><h1><?php echo $this->lang->line('contact_value'); ?></h1></div>

    <div class="row">

        <div class="col">
            <br/>
            <br/>
            <img src="assets/images/contact-us.jpg" alt="contact-us" class="img-thumbnail img-responsive">
        </div>

        <div class="col">
            </br>

            <?php echo form_open(); ?>

            <div class="form-group">
                <label for="InputName"><strong><?php echo $this->lang->line('name'); ?></strong></label>
                <input type="text" name="name" class="form-control" id="InputName" placeholder="<?php echo $this->lang->line('input_name'); ?>">
            </div>
            <div class="form-group">
                <label for="company"><strong><?php echo $this->lang->line('company'); ?></strong></label>
                <input type="text" name="company" class="form-control" id="InputCompanyName" placeholder="<?php echo $this->lang->line('input_company_name'); ?>">
            </div>
            <div class="form-group">
                <label for="InputEmail"><strong><?php echo $this->lang->line('email'); ?></strong></label>
                <input type="text" name="contact" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="<?php echo $this->lang->line('input_email'); ?>">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="SelectService"><strong><?php echo $this->lang->line('service'); ?></strong></label>
                <select class="form-control" name="services" id="SelectService">
                    <option value="all"><?php echo $this->lang->line('all'); ?></option>
                    <option value="ramp"><?php echo $this->lang->line('ramp'); ?></option>
                    <option value="passenger"><?php echo $this->lang->line('passenger'); ?></option>
                    <option value="crew"><?php echo $this->lang->line('crew'); ?></option>
                    <option value="meetandassist"><?php echo $this->lang->line('assist'); ?></option>
                    <option value="other"><?php echo $this->lang->line('other'); ?></option>
                </select>
            </div>
            <div class="form-group">
                <label for="subject"><strong><?php echo $this->lang->line('subject'); ?></strong></label>
                <textarea id="subject" name="subject" class="form-control" placeholder="<?php echo $this->lang->line('input_subject'); ?>" ></textarea>


            </div>
            </br>

            <div class="row">
                <div class="col"></div>
                <div class="col">
                    <!--<button type="submit" class="btn btn-primary btn-block" action="mailto:z.rakotonindriana@mg-handling.com@mg-handling.com?Subject=From website"><?php echo $this->lang->line('submit'); ?></button>-->
                    <input type="Submit" class="btn btn-primary btn-block" value="Envoyer">
                </div>
                <div class="col"></div>

            </div>
            <?php echo form_close(); ?>

            </br>
            </br>
            </br>
            </br>
            </br>



        </div>


    </div>

</div>
