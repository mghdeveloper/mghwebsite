
<div class="container">
<br />
<br />
<br />
<br />
<br />

    <div class="col offset-4">

        <div class="row">
            <div class="col-md-4 col-md-offset-4 card">
                <div class="card-body">
                    </br>
                    <div>
                        <h3 class="panel-title"> <img src="<?php echo img_url('logo/medium-mgh.jpg'); ?> " class="img-responsive img-fluid img-thumbnail" />
                        </h3>
                    </div>
                    </br>
                    <div>
                        <form role="form" action="<?php echo site_url('admin/login');?>" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Login" name="pseudo" type="text" autofocus value="" >
                                </div>
                                <i class="text-danger"></i>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="mdp" type="password" value="">
                                </div>
                                <i class="text-danger"></i>

                                <button type="submit" class="btn btn-lg btn-success btn-block">Connexion</button>
                                <br>
                                <br>
                                <div class="text-center">
                                    <i class=" glyphicon glyphicon-hand-right"></i><a href="<?php echo site_url('home'); ?>"> <i class="fa fa-hand-o-left fa-2x"></i> Return to the home page</a>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>