<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Website is under Maintenance</title>

    <style type="text/css">

        ::selection{ background-color: #E13300; color: white; }
        ::moz-selection{ background-color: #E13300; color: white; }
        ::webkit-selection{ background-color: #E13300; color: white; }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-top: 1px solid #D0D0D0;
            font-size: 22px;
            font-weight: bolder;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;

        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #body{
            margin-right: auto;
            margin-left: auto;


        }

        p.footer{
            text-align: right;
            font-size: 11px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container{
            margin-left: 25%;
            margin-right: 25%;
            margin-top: 10%;
            border: 1px solid #D0D0D0;
            -webkit-box-shadow: 0 0 8px #D0D0D0;
            text-align: center;
        }
    </style>
    <link rel="icon" href="<?php echo img_url('favicon.ico'); ?>">
</head>
<body>

<div id="container">
    <img src="<?php echo img_url('logo-big.png') ;?>">


    <div id="body">

        <h1>Site Under Maintenace</h1>

        <img src="<?php echo img_url('cog.png') ;?>">
        <p><strong>Madagascar Ground Handling Website</strong> is under maintenance.We should be back shortly. Thank you for your patience.  </p>

    </div>

    <p class="footer">Website Administrator</p>
</div>

</body>
</html>