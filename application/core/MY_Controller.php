<?php
/**
 * Created by PhpStorm.
 * User: Andrianina
 * Date: 19/12/2014
 * Time: 12:59
 */

class MY_Controller extends CI_Controller{

    public function __construct()
    {
        parent::__construct();

        if($this->config->item('maintenance_mode') == TRUE) {
            redirect('maintenance');
        }
    }
} 