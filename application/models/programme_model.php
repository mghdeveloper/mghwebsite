<?php
/**
 * Created by PhpStorm.
 * User: Andrianina
 * Date: 05/02/2015
 * Time: 14:33
 */

class Programme_model extends  CI_Model {

    private $table='programme_base';






    public function insert_programme($reference,$operator,$actype,$acregn,$callsign,$purposeflight,$pax,$crew,$load,$services,$addpost,$date1,$date2,$dep1,$dep2,$ar1,$ar2,$etd1,$etd2,$eta1,$eta2,$codecli){

        return $this->db->set('REFERENCE_VOL_PROGRAMME_BASE',$reference)
                        ->set('OPERATEUR_PROGRAMME_BASE',$operator)
                        ->set('AIRCRAFT_TYPE_PROGRAMME_BASE',$actype)
                        ->set('AIRCRAFT_REGISTRATION_PROGRAMME_BASE',$acregn)
                        ->set('CALL_SIGN_PROGRAMME_BASE',$callsign)
                        ->set('POF_FLIGHT',$purposeflight)
                        ->set('PAX_PROGRAMME_BASE',$pax)
                        ->set('CREW_PROGRAMME_BASE',$crew)
                        ->set('LOAD_PROGRAMME_BASE',$load)
                        ->set('REQUEST_PROGRAMME',$services)
                        ->set('ADDITIONAL_PROGRAMME',$addpost)
                        ->set('DATE_VOL_PROGRAMME_BASE',$date1)
                        ->set('DATE2_VOL_PROGRAMME_BASE',$date2)
                        ->set('DEP1_PROGRAMME_BASE',$dep1)
                        ->set('DEP2_PROGRAMME_BASE',$dep2)
                        ->set('ARR1_PROGRAMME_BASE',$ar1)
                        ->set('ARR2_PROGRAMME_BASE',$ar2)
                        ->set('ETD_PROGRAMME_BASE',$etd1)
                        ->set('ETD2_PROGRAMME_BASE',$etd2)
                        ->set('ETA_PROGRAMME_BASE',$eta1)
                        ->set('ETA2_PROGRAMME_BASE',$eta2)
                        ->set('CODE_CLIENT',$codecli)
                        ->set('DATE_AJOUT_PROGRAMME','NOW()',false)
                        ->set('ETAT_PROGRAMME',false)
                        ->insert($this->table);

    }


    public function get_list_order(){

        return $this->db->select('*')
            ->from($this->table)
            ->order_by('DATE_AJOUT_PROGRAMME','DESC')
            ->get()
            ->result();

    }


    public function get_list_ordercli($codecli){

        return $this->db->select('*')
            ->from($this->table)
            ->where('CODE_CLIENT',$codecli)
            ->order_by('DATE_AJOUT_PROGRAMME','DESC')
            ->get()
            ->result();

    }




    public function get_message_message_by_ID($val){

        return $this->db->select('*')
            ->from($this->table)
            ->where('CODE_PROGRAMME_BASE',$val)
            ->get()
            ->result();




    }



    public function count_contact_non_lu()
    {
        return (int) $this->db->where('ETAT_PROGRAMME',0)
            ->count_all_results($this->table);
    }


    public function update_etat($val){

        $this->db->set('ETAT_PROGRAMME',true);
        $this->db->where('CODE_PROGRAMME_BASE', (int) $val);
        return $this->db->update($this->table);

    }






}