<!DOCTYPE html>
<html>
<head>
    <title><?php echo $titre; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php
    echo $charset; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo img_url('favicon.ico'); ?>">
    <style>
        .well.special{
            background-color:#000000;
            border: 1px solid #000000;

        }

        p.souligne{

            text-decoration: underline;
            font-size: 20px;
            font-weight:bold;
        }

        i.souligne2{

            text-decoration: underline;
            font-size: 18px;
            font-weight:bold;
        }

        p.heading{

            font-size: 18px;
            font-weight:bold;

        }

        ul.custom-list{

            list-style-type: none;
        }


        small{
            font-style: italic;
        }

        a{
            text-decoration: underline;
            color: darkslateblue;
        }

        a:hover{
            color: darkslateblue;
        }
    </style>

    <?php foreach($css as $url): ?>
        <link rel="stylesheet" type="text/css" href="<?php
        echo $url; ?>" />
    <?php endforeach; ?>


</head>
<body>

<?php echo $output; ?>

<footer>

</footer>



<?php foreach($js as $url): ?>
    <script type="text/javascript" src="<?php echo $url; ?>"></script>
<?php endforeach; ?>


</body>
</html>