<!DOCTYPE html>
<html>
<head>
    <title><?php echo $titre; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $charset; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon" />


    <link rel="stylesheet" type="text/css" href="<?php echo css_url('bootstrap.min'); ?>">
    <link href="<?php echo css_url('font-awesome.min'); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo css_url('ie10-viewport-bug-workaround'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo css_url('style'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo css_url('half-slider'); ?>">
    <?php foreach($css as $url): ?>
        <link rel="stylesheet" type="text/css" href="<?php
        echo $url; ?>" />
    <?php endforeach; ?>


</head>
<body class="body">

					<?php echo $output; ?>
	
    <!-- Footer -->
    <footer class="footer py-1 bg-dark">


        <div class="container">
            <div class="row">
                <div class="col">

                            <p class="small text-center text-primary">
                                Madagascar Ground Handling (MGH)<br />
                                Aéroport International d'Ivato <br /> 1er étage mezzanine Terminal A -
                                Antananarivo 00105 <br /> MADAGASCAR BP: 152

                            </p>

                </div>
                <div class="col">
                      <p class="m-0 text-center text-primary align-content-center"><i class="fa fa-3x fa-plane"></i> <br /> Copyright &copy; MAdagascar Ground Handling </p>
                </div>
                <div class="col">
                      <a href="mailto:sales@mg-handling.com"><p class="m-0 text-center text-primary align-content-center"> <i class="fa fa-3x fa-envelope"></i> <br /> sales@mg-handling.com</p></a>
                </div>
                
                <div class="col">
                    <img src="assets/images/ISAGO.jpg" alt="Certification ISAGO" class="img-responsive img-thumbnail">
                </div>



            </div>
        </div>

        <!-- /.container -->
    </footer>

<script type="text/javascript" src="<?php echo js_url('jquery.min'); ?>"></script>
<script type="text/javascript" src="<?php echo js_url('bootstrap.bundle.min'); ?>"></script>
<script type="text/javascript" src="<?php echo js_url('html5shiv.min'); ?>"></script>
<script type="text/javascript" src="<?php echo js_url('ie10-viewport-bug-workaround'); ?>"></script>
<?php foreach($js as $url): ?>
    <script type="text/javascript" src="<?php echo $url; ?>"></script>
<?php endforeach; ?>


</body>
</html>