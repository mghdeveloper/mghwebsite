<!DOCTYPE html>
<html>
<head>
    <title><?php echo $titre; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php
    echo $charset; ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo img_url('favicon.ico'); ?>" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php echo img_url('favicon.ico'); ?>" type="image/x-icon" />


    <!-- Bootstrap Core CSS -->
    <link href="<?php echo css_url('bootstrap'); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo css_url('font-awesome.min'); ?>" rel="stylesheet" type="text/css">

    <?php foreach($css as $url): ?>
        <link href="<?php echo $url; ?>" rel="stylesheet"  />
    <?php endforeach; ?>
    <?php if(isset($css_files))
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>


</head>
<body class="bg-light">



    <?php echo $output; ?>



<script type="text/javascript" src="<?php echo js_url('jquery-1.11.1'); ?>"></script>
<script type="text/javascript" src="<?php echo js_url('jquery-migrate-1.2.1'); ?>"></script>
<script type="text/javascript" src="<?php echo js_url('bootstrap.min'); ?>"></script>
<?php if(isset($js_files))
   foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php foreach($js as $url): ?>
    <script type="text/javascript" src="<?php echo $url; ?>"></script>
<?php endforeach; ?>

</body>
</html>