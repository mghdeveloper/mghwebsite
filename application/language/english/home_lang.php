<?php
/**
 * Created by PhpStorm.
 * User: Castiel
 * Date: 05/11/14
 * Time: 14:09
 */

/**
 *
 * HOME (ENG)
 *
 */



// ---------------- SLIDE 1 --------------------------

$lang['society_name'] = "Madagascar Ground Handling";
$lang['society_slogan'] = "Your Handling Agent in Madagascar";

// ---- ancien texte slide 1 ---- //

$lang['society_motto'] = "We offer High Standard Punctual services, to Support Tourism as the most important industry of the country and to Achieve our company long term growth plan.";
$lang['society_rh'] = "Human Ressources Departement";


// ---- nouveau texte slide 1 ---- //

$lang['slide1_text'] = <<< EOT
Need to fly to Madagascar? We are the solution. </br>
<strong>Madagascar Ground Handling</strong> is proud to serve your Company. We provide high quality standard services in most of the cities of Madagascar.
EOT;

$lang['society_aboutus'] = "More about us";



// ---------------- SLIDE 2 --------------------------


$lang['service_content'] = <<< EOT

<p><strong class="text-primary">MADAGASCAR GROUND HANDLING</strong> provides services in accordance with latest <strong class="text-primary">IATA</strong> standards.</p>

<p>The strength of the company is based on its anticipation and the staff reactivity towards each of its customers particular needs regarding  <strong class="text-primary">passengers, aircraft, baggages and freight handling</strong>.</p>

<p>Our Staff is <strong class="text-warning">regularly trained</strong> and qualified in order to keep the standard level. Our training are <strong class="text-warning">IATA certified</strong>.<p/>

EOT;



$lang['flt'] = "Flights";
$lang['psr'] = "Passengers";
$lang['cgo'] = "Tons of Cargo";





// ---------------- SLIDE 3 --------------------------






$lang["customer_value_content"] ="<p>We are proud to serve amongst the best airlines in the world.</p>";

// ---- ancien texte slide 3 ---- //

$lang["contenu_panel_network"] = "Leader in ground handling in the island, <strong>Madagascar Ground Handling</strong> provides his services in the 12 major airports of Madagascar.<br/><br/>";
$lang["contenu_panel_services"] = "<strong>Madagascar Ground Handling</strong> provides aircraft ground handling for the major airlines serving Madagascar. It has trained staffs that have the required qualifications to enable it to fully meet the needs of its customers in compliance with international standards Safety operation.<br/><br/>";
$lang["contenu_panel_certifications"] = "<strong>Madagascar Ground Handling</strong> provides services that meet international safety standards.";

// ---- nouveau texte slide 3 ---- //

$lang["slide3-text"] = <<< EOT
Leader in ground handling in the island, <strong>Madagascar Ground Handling</strong> provides its services to all international flights to and from Madagascar, thanks to all its partners through the Island.</br></br>

<strong>Madagascar Ground Handling</strong> aims to fully meet the needs of its customers in compliance with international Safety standards.
EOT;

// -- bouton slide 3 -- //

$lang["button-customers"] = "Our Customers";







// ---------------- SLIDE 4 --------------------------


$lang["contenu_well_home"] = <<< EOT

Madagascar Ground Handling (MGH) is pleased to announce that it has obtained the IATA Safety Audit for Ground Operations (ISAGO) certification for its headquarters based at Ivato Antananarivo International Airport, as of 02 November 2018 .<br />
The acquisition of the ISAGO label has been a goal of MGH since its creation in 2016, because it is a guarantee of compliance with international standards, thus strengthening the trust of our Airlines partners.<br />
The IATA Safety Audit for Ground Operations (ISAGO) is based on a "foundation" of auditing standards applicable to all ground support companies around the world, combined with a uniform set of standards relevant to specific activities of any ground assistance provider.<br />
This certification, valid for two years, covers 06 areas of Organization and Management (ORM), Load Control (LOD), Passenger and Baggage Handling (PAB), Aircraft Handling and Loading (HDL), Aircraft Ground Movement (AGM) and Cargo and Mail Handling (CGM). It confirms that MGH's ground handling services comply with international standards.<br />
The achievement of this result is the result of the effort provided by each staff member within MGH. Mrs. Noro RAZAFIMAHEFA, Managing Director, said: "This distinction is a reward for the work of everyone and represents a real opportunity to better manage operational risks and the safety of ground services".

EOT;

$lang["contenu_bouton_well_home"] = "Contact Us";

$lang["contenu_title_well_home"] = " Certification ISAGO";










$lang['offer']="What can we offer ?";
$lang['value']="We are proud to serve amongst the best airlines in the world";
$lang['service'] = "We are at your service";


//about lang
$lang['title_aboutus'] = '<h1 class="badge  badge-warning txt-s4">Madagascar Ground Handling at a glance </h1>';




$lang['previous'] = "previous";
$lang['next'] = "next";


$lang["contenu_read_more"] ="Read more";