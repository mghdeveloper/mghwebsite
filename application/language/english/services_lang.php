<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// ------------- HEADER -------------
$lang['sv_page_title'] = "Our Services";

// ------------- RAMP SERVICE -------------

$lang["sv_ramp_title"] = "Ramp Services";
$lang['sv_ramp_content']= <<< EOT

<p class="text-justify">With various ground support equipments (GSE), we are servicing numerous types of aircraft from Twin Otter to B747 passenger and cargo aircraft.</p>
<p class="text-justify">According to your turnaround time and your schedule and in a safely manner, we perform Load control, Weight and Balance, Flight Dispatch, Baggage loading and unloading, Ramp Safety, ULD local control ... </p>
<p class="text-justify">We also provide an office on airside for your crew briefing. <br />
<p class="text-justify">We are certified on main DCS such as Amadeus Altea &copy;, Sabre &copy;, Troya &copy; as well as manually.</p>

EOT;

// ------------- PASSANGER SERVICE -------------
$lang["sv_psr_title"] = "Passenger Services";
$lang['sv_psr_content']= <<< EOT
    Satisfaction of your customers is in the heart of our activities. Our well-trained staff is delighted to provide a seamless experience to your passengers and crew by facilitating the process from check-in queue to boarding. <br />
    We also provide ticketing and excess baggage solutions at the airport. <br />
    A special assistance is also provided to passengers with special need such as wheelchair, information assistance, Meet-And-Assist.
    We are certified on main DCS such as Amadeus Altea &copy;, Sabre &copy;, Troya &copy; as well as manually. We can also provide automated handling through our Altea GUI if Customer does not have handling system in place.

EOT;

// ------------- BAGGAGE SERVICES -------------
$lang["sv_baggage_title"] = "Baggage Services";
$lang['sv_baggage_content']= <<< EOT
    We handle Lost and Found at the Airport.<br />
    We are certified on Worldtracer &copy;.

EOT;



// ------------- CARGO SERVICE -------------
$lang["sv_cargo_title"] = "Cargo Services";
$lang['sv_cargo_content']= <<< EOT
    Our experienced team will be handling our cargo in a safely manner. We provide services from the Ramp to the Warehouse, till delivery to consignee. As for exportation, we also perform sales, acceptation, palletization, and loading to the aircraft.
    Permanent communication between our Cargo staff and flight coordinator on the Ramp provides a safely and timely manner to respect aircraft turnaround schedule.

EOT;


// ------------- LOUNGE -------------
$lang["sv_lounge_title"] = "Lounge Services";
$lang['sv_lounge_content']='Our VIP lounge will let your passenger relax and be stress free. A complementary snack is provided.';



// ------------- MEET AND ASSIST SERVICE -------------
$lang["sv_assist_title"] = "Meet and Assist Services";
$lang['sv_assist_content']= <<< EOT
    Whenever you are arriving, in transit or departing from Madagascar, our Meet-And-Assist service will dedicate you a special staff for assistance. It can both include passengers, crew as well as aircraft authorizations and documents. <br />
    A seamless experience, thanks to our team and our partners.

EOT;


// ------------- CUSTOMIZED SERVICE -------------
$lang["sv_customize_title"] = "Customized Services";
$lang['sv_customize_content']='...';//todo: à définir services personnalisés


$lang['service_pride']='We are dedicated to the service of our Customers.';
$lang['service_listing']='These are the Services we provide:';
