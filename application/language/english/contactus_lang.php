<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* tab */
$lang['call_center'] = 'Call center and Useful numbers';
$lang['contact'] = 'Contact Us';





$lang['contact_value'] = "We are glad to help";
$lang['name'] = "Your Name";
$lang['company'] = "Your Company";
$lang['email'] = "Your Email address";
$lang['service'] = "Services";
$lang['all'] = "All";
$lang['ramp'] = "Ramp Services";
$lang['passenger'] = "Passenger Services";
$lang['baggage'] = "Baggage Services";
$lang['cargo'] = "Cargo Services";
$lang['lounge'] = "Lounge Services";
$lang['meet'] = "Meet And Assist Services";
$lang['custom'] = "Customized Services";
$lang['crew'] = "Crew Services";
$lang['assist'] = "Meet-and-Assist";
$lang['other'] = "Other Services";
$lang['subject'] = "Subject";

/* Bouton */
$lang['submit'] = "Submit";

/* Input Name */
$lang['input_name'] = "Please enter your name";

/* Input Company Name */
$lang['input_company_name'] = "Please enter your company name ";

/* Input Mail */
$lang['input_email'] = "Please enter your email";

/* Input Subject */
$lang['input_subject'] = "Please write your message";