<?php
/**
 * Created by PhpStorm.
 * User: MAEVA
 * Date: 07/05/18
 * Time: 10:06
 */

/**
 *
 * CAREERS (ENG)
 *
 */


$lang["career_title"] = "Join Us";

$lang["career_content"] = <<< EOT

<p>At <strong>MADAGASCAR GROUND HANDLING</strong> we challenge our employees to be leaders in our industry.</p>
<p> This philosophy is embodied in our 4 Core Values : <strong class="text-warning">INTEGRITY</strong>, <strong class="text-warning">TEAM SPIRIT</strong>, <strong class="text-warning">VERSATILITY</strong> et <strong class="text-warning">DYNAMISM</strong>.</p>


<p>Our <strong>Integrity</strong> make the difference in our business.<br>
Join us and help be a part of that difference.</p>

<p>Our <strong>Team Spirit</strong> boost our productivity.<br>
Join the team and take part in the adventure.</p>


<p>Our <strong>Versatility</strong> and <strong>Dynamism</strong> sets us apart from our competition.<br>
Join us and be the reference for the industry.</p>


<p>We are engaged in Partnerships with  major airlines in the world.<br/>
Join us and be a part of our global network.</p>



EOT;

/* formulaire */
$lang['last_name'] = 'Your Last Name';
$lang['input_last_name'] = 'Please enter your Last Name';
$lang['first_name'] = 'Your First Name';
$lang['input_first_name'] = 'Please enter your First Name';
$lang['CV'] = 'Your CV';
$lang['application_letter'] = 'Your application letter';