<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * CUSTOMER ( ENG )
 *
 */

$lang['content_head_custom'] = <<< EOT

Thanks to all our Customer for your trust in our services!</br>
At MGH, we serve both regular flights and private ones. Commercial as well as military flights are most welcome.

EOT;

$lang['regular'] = "On Regular Basis";
$lang['adhoc'] = "On ad-hoc Basis";