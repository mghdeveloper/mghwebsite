<?php
/**
 * Created by PhpStorm.
 * User: Andrianina
 * Date: 27/04/2018
 * Time: 11:59
 */



/* ----- MENU TOP BAR (ENG) ------ */

$lang["nav_home"] = "Home";
$lang["nav_network"] = "Our Network";
$lang["nav_services"] = "Our Services";
$lang["nav_certifications"] ="Our Certifications";
$lang["nav_customer"] ="Our Customers";
$lang["nav_aboutus"] ="About Us";
$lang["nav_careers"] ="Careers";
$lang["nav_contactus"] ="Contact Us";
