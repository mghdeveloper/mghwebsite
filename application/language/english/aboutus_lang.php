<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *  ABOUT US (ENG)
 *
 */


// ---------------- SECTION HISTORY  ----------------

$lang['title_history'] = 'Madagascar Ground Handling at a glance';
$lang['content_history'] = 'Created in 2016, we are a proud part of Madagascar welcoming Country.';


// ---------------- SECTION PRIDE  ----------------

$lang['title_pride'] = 'Mission, Vision, Values';
$lang['content_pride'] = <<< EOT

<p>Our vision is to gain your fidelity through passion, efficiency and innovation. </p>

<p>First image at the gates of Madagascar by air, the MGH family brings together people of character and dedicated to the service of the Customer. </p>

<p><em>Our values are based on <strong>integrity</strong>,<strong> team spirit</strong>,<strong> skilfulness</strong> and <strong>dynamism</strong>.</em></p>

EOT;

// ---------------- SECTION TEAM  ----------------

$lang['title_team'] = 'Our Team';
$lang['name'] = "Name";
$lang['photo'] = "Photo";
$lang['job_ceo'] = "Chief Executive Officer";
$lang['job_op'] = "Operations Director";
$lang['job_afd'] = "Administrative and Finance Director";
$lang['job_lid'] = "Logistics and IT Director";

// ---------------- SECTION BOOKLET  ----------------

$lang['title_booklet'] = 'Our Brochure';
$lang['content_booklet'] = 'To know more about us, please download our Brochure here (pdf file reader needed):';

$lang['booklet-link'] = 'Our Brochure';
