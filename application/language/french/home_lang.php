<?php
/**
 * Created by PhpStorm.
 * User: Castiel
 * Date: 05/11/14
 * Time: 14:09
 */

/***
 *
 *  HOME (FR)
 *
 */



// ---------------- SLIDE 1 --------------------------

$lang['society_name'] = "Madagascar Ground Handling";
$lang['society_slogan'] = "Votre Service d'assistance au Sol à Madagascar";

// ---- ancien texte slide 1 ---- //

$lang['society_motto'] = "Nous vous offrons des services ponctuels de haut niveau afin de soutenir le Tourisme qui est l'une des activités majeures du pays et aider au développement de la compagnie.";
$lang['society_rh'] = "Département Ressources Humaines";


// ---- nouveau texte slide 1 ---- //

$lang['slide1_text'] = <<< EOT
<strong>Madagascar Ground Handling</strong> est fière de vous fournir les services d'assistance au Sol dans les normes internationales et en toute Sécurité.</br>
Nos prestations s'étendent sur les principales villes de Madagascar.
EOT;

$lang['society_aboutus'] = "En savoir plus";



// ---------------- SLIDE 2 --------------------------


$lang['service_content'] = <<< EOT

<p><strong class="text-primary">MADAGASCAR GROUND HANDLING</strong> fournit à ses clients une qualité de services en accord avec les normes <strong class="text-primary">IATA</strong>.</p>

<p>La force de la compagnie s'appuie sur son anticipation ainsi que sur la réactivité du personnel face aux besoins particuliers de chacun de ses clients en ce qui concerne l' <strong class="text-primary">assistance des passagers</strong>, le <strong class="text-primary">traitement des avions, des bagages</strong> et des <strong class="text-primary">cargos</strong>.</p>

<p>Des <strong class="text-warning"> formations </strong> sont dispensées à intervalles réguliers afin de maintenir la qualification du personnel de la compagnie.<p/>

EOT;


$lang['offer']="Que pouvons nous offrir ?";


$lang['flt'] = "Flights";
$lang['psr'] = "Passengers";
$lang['cgo'] = "Tons of Cargo";



// ---------------- SLIDE 3 --------------------------




//$lang['value']="We are proud to serve amongst the best airlines in the world";



$lang["customer_value_content"] = <<< EOT
Nous sommes fiers de servir parmi les meilleures compagnies aériennes </br>
du monde.

EOT;

// ---- ancien texte slide 3 ---- //

$lang["contenu_panel_network"] = "Leader dans l'assistance en escale dans la grande île, <strong>Madagascar Ground Handling</strong> est présent dans les 12 principaux aéroports de Madagascar.</br></br>";
$lang["contenu_panel_services"] = "<strong>Madagascar Ground Handling</strong> assure le traitement au sol des avions des grandes compagnies aériennes desservant Madagascar. Elle dispose de personnels formés qui ont les qualifications requises pour lui permettre de répondre pleinement aux besoins de ses clients dans le respect des normes internationales de sécurité des opérations.</br></br>";
$lang["contenu_panel_certifications"] = "<strong>Madagascar Ground Handling</strong> fournit des services répondant aux normes internationales de sécurité.";

// ---- nouveau texte slide 3 ---- //

$lang["slide3-text"] = <<< EOT
Leader dans l'assistance en escale dans la grande île, <strong>Madagascar Ground Handling</strong> fournit ses prestations à la majorité des Compagnies aériennes internationales grâce à son expérience mais également à ses partenaires.</br></br>

<strong>Madagascar Ground Handling</strong> ambitionne de satisfaire ses Clients tout en respectant l'horaire dans les normes de Qualité, Sécurité et Sûreté requises.
EOT;

// -- bouton slide 3 -- //

$lang["button-customers"] = "Nos clients";



















//**************************1 well homr ********************



$lang["contenu_well_home"] = <<< EOT

Madagascar Ground Handling (MGH) est ravie d’annoncer l’obtention de la certification ISAGO (IATA Safety Audit for Ground Operations) de son siège social et de son escale à l’aéroport international d’Ivato Antananarivo, à compter du 02 Novembre 2018.<br />
L’acquisition du label ISAGO fait partie des objectifs de la société MGH depuis sa création en 2016, car elle est un gage de respect des normes et standard internationaux permettant ainsi de renforcer la confiance des Compagnies Clientes.< br />
L’IATA Safety Audit for Ground Operations (ISAGO) s’appuie sur un « socle » de normes d’audit applicables à toutes les entreprises de services d’assistance au sol du monde entier, associées à un ensemble uniforme de normes pertinentes pour les activités spécifiques de tout fournisseur d’assistance au sol.<br />
Cette certification, valable pour deux années, couvre 06 domaines que sont Organization and Management (ORM), Load Control (LOD), Passenger and Baggage Handling (PAB), Aircraft Handling and Loading (HDL), Aircraft Ground Movement (AGM) et Cargo and Mail Handling (CGM). Elle confirme que les services d’assistance en escale de MGH sont conformes aux normes internationales.<br />
L’atteinte de ce résultat est le fruit de l’effort fourni par chaque membre du personnel au sein de MGH. Madame Noro RAZAFIMAHEFA, Directeur Général, a déclaré : « Cette distinction est une récompense du travail de tout un chacun et représente une véritable opportunité pour mieux gérer les risques opérationnels et la sécurité des services au sol ».

EOT;

$lang["contenu_bouton_well_home"] = "Contactez-Nous";
$lang["contenu_title_well_home"] = "Certification ISAGO";

//about us lang
$lang['title_aboutus'] = '<h1 class="badge  badge-warning txt-s4">Madagascar Ground Handling at a glance </h1>';

$lang['previous'] = "previous";
$lang['next'] = "next";



$lang["contenu_read_more"] ="En savoir plus";