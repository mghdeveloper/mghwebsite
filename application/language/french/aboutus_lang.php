<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * ABOUT US (FR)
 *
 */


// ---------------- SECTION HISTORY  ----------------

$lang['title_history'] = 'Madagascar Ground Handling at a glance';
$lang['content_history'] = 'Créé en 2016, nous sommes fiers de faire partie du patrimoine de Madagascar.';


// ---------------- SECTION PRIDE  ----------------

$lang['title_pride'] = 'Mission, Vision, Values';
$lang['content_pride'] = <<< EOT

<p>Notre vision est de gagner votre fidélité à travers la passion et l'innovation. </p>

<p>Première image aux portes de Madagascar par les airs, la famille MGH comporte des personnes de caractère et dédiées au service du Client. </p>

<p><em>Nos valeurs sont l'<strong>Intégrité</strong>, l'<strong>esprit d'équipe</strong>, la <strong>polyvalence</strong> et le <strong>dynamisme</strong>.</em></p>

EOT;

// ---------------- SECTION TEAM  ----------------


$lang['title_team'] = 'Notre Equipe';
$lang['name'] = "Name";
$lang['photo'] = "Photo";
$lang['job_ceo'] = "Directeur Général";
$lang['job_op'] = "Directeur d'Exploitation";
$lang['job_afd'] = "Directeur Administratif et Financier";
$lang['job_lid'] = "Directeur  Logistique et Système d'Information";


// ---------------- SECTION BOOKLET  ----------------

$lang['title_booklet'] = 'Notre Brochure';
$lang['content_booklet'] = 'Téléchargez notre brochure pour nous connaître un peu plus et voir ce que nous pouvons faire pour satisfaire vos attentes.';
$lang['booklet-link'] = 'Notre Brochure';