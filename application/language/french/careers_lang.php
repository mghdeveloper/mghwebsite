<?php
/**
 * Created by PhpStorm.
 * User: MAEVA
 * Date: 07/05/18
 * Time: 10:08
 */

/**
 *
 * CAREERS (FR)
 *
 */


$lang["career_title"] = "Rejoignez-Nous";

$lang["career_content"] = <<< EOT


Appartenir à <strong>Madagascar Ground Handling (MGH)</strong>, c’est appartenir à une Société vitrine de Madagascar.</br>
Notre vision est de gagner la fidélité par la passion et l’innovation. </br>
Première image aux portes de Madagascar par voie aérienne, la famille MGH réunit des personnes de caractère et dédiés au service du Client. </br></br>

Cette philosophie est encrée dans nos 4 valeurs principales : <strong class="text-warning">INTEGRITE</strong>, <strong class="text-warning">ESPRIT D'EQUIPE</strong>, <strong class="text-warning">POLYVALENCE</strong> et <strong class="text-warning">DYNAMISME</strong>.</br></br>

Notre <strong>Intégrité</strong> fait la différence dans notre secteur d'activité.</br>
Rejoignez-nous et prenez part à cette différence.</br></br>

Notre <strong>Esprit d'Equipe</strong> renforce notre productivité.</br>
Rejoignez l'équipe et prenez part à l'aventure.</br></br>

Notre <strong>Polyvalence</strong> et notre <strong>Dynamisme</strong> nous démarquent de notre concurrence.</br>
Rejoignez-nous et devenez une référence pour l'industrie.</br></br>

Nous avons des partenariats avec de grandes compagnies aériennes dans le monde.</br>
Rejoignez-nous et devenez membre de notre réseau.


EOT;

/* formulaire */
$lang['first_name'] = 'Votre Prénom';
$lang['input_first_name'] = 'Veuillez saisir votre Prénom';
$lang['last_name'] = 'Votre Nom';
$lang['input_last_name'] = 'Veuillez saisir votre Nom';
$lang['CV'] = 'Votre CV';
$lang['application_letter'] = 'Votre lettre de motivation';