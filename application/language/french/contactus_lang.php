<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* tab */

$lang['call_center'] = 'Call center et numéros utiles';
$lang['contact'] = 'Nous contacter';

$lang['contact_value'] = "Nous sommes à votre disposition";
$lang['name'] = "Votre Nom";
$lang['company'] = "Votre Compagnie";
$lang['email'] = "Votre adresse email";
$lang['service'] = "Les Services";
$lang['all'] = "Tout";
$lang['ramp'] = "Les Services en Piste";
$lang['passenger'] = "Les Services aux Passagers";
$lang['baggage'] = "Les Services Baguages";
$lang['crew'] = "Les Services pour les Equipages";
$lang['assist'] = "Meet-and-Assist";
$lang['other'] = "Autres Services";
$lang['subject'] = "Objet";

/* Bouton */
$lang['submit'] = "Envoyer";

/* Input Name */
$lang['input_name'] = "Veuillez saisir votre nom";

/* Input Company Name */
$lang['input_company_name'] = "Veuillez saisir le nom de votre compagnie";

/* Input Mail */
$lang['input_email'] = "Veuillez saisir votre adresse email";

/* Input Subject */
$lang['input_subject'] = "Veuillez écrire votre message";
