<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * CUSTOMER ( FR )
 *
 */


$lang['content_head_custom'] = '<blockquote class="blockquote">Merci à tous nos Clients pour leur confiance en nos services!
            MGH traite tant autant les vols réguliers que les vols ponctuels. Les vols commerciaux ainsi que les vols militaires sont les bienvenus.</blockquote>';
$lang['regular'] = "Vols réguliers";
$lang['adhoc'] = "Vols ponctuels";