<?php
/**
 * Created by PhpStorm.
 * User: Andrianina
 * Date: 27/04/2018
 * Time: 12:00
 */



/* ----- MENU TOP BAR (FR) ------ */

$lang["nav_home"] = "Accueil";
$lang["nav_network"] = "Notre Réseau";
$lang["nav_services"] = "Nos Services";
$lang["nav_certifications"] ="Nos Certifications";
$lang["nav_customer"] ="Nos Clients";
$lang["nav_aboutus"] ="À propos de nous";
$lang["nav_careers"] ="Carrières";
$lang["nav_contactus"] ="Contactez Nous";
