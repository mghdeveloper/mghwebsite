<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// ------------- HEADER -------------
$lang['sv_page_title'] = 'Nos Services';

// ------------- RAMP SERVICE -------------

$lang["sv_ramp_title"] = "Les Services en Piste";
$lang['sv_ramp_content']= <<< EOT

<p class="text-justify">Avec l'aide de plusieurs équipements de piste, nous traitons plusieurs types d'avions allant du Twin Otter au B747 passenger, ainsi que les avions cargo.</p>
<p class="text-justify">Selon la durée du transit ainsi que votre programme des vols, et dans les normes de Sécurité et de Sûreté, nous effectuons le Load control, Weight and Balance, Flight Dispatch, le chargement/déchargement/litiges bagages, les opérations en piste, la vérification des conteneurs/palettes (ULD), ... . </p>
<p class="text-justify">Nous disposons également d'un bureau côté piste pour les briefings des membres d'équipage. <br />
<p class="text-justify">Nous sommes certifiés sur les principaux DCS tels que Amadeus Altea &copy;, Sabre &copy;, Troya &copy;. Mais nous effectuons également le traitement manuel si nécessaire.</p>

EOT;

// ------------- PASSANGER SERVICE -------------
$lang["sv_psr_title"] = "Les Services aux Passagers";
$lang['sv_psr_content']= <<< EOT
    La Satisfaction de nos Clients est au coeur de notre activité. Notre équipe qualifiée a le plaisir de vous fournir une agréable expérience autant pour vos passagers que pour vos membres d'équipage, en facilitant tout le processus de traitement à l'arrivée, comme au départ et durant les transit. </br></br>
    Nous proposons également la vente de billets et la taxation d'excédents de bagages à l'aéroport. </br></br>
    Une assistance spécifique est fournie aux passagers ayant des besoins spécifiques tels que des chaises roulantes, informations spécifiques, Meet-And-Assist.
    Nous sommes certifiés sur les principaux DCS tels que Amadeus Altea &copy;, Sabre &copy;, Troya &copy;, ainsi que manuellement si nécessaire. Nous pouvons également vous fournir un traitement automatisé de votre vol à travers le système Altea.

EOT;


// ------------- BAGGAGE SERVICES -------------
$lang["sv_baggage_title"] = "Les Services Bagages";
$lang['sv_baggage_content']=' Nous disposons d\'un Service Litige Bagages à l\'aéroport et sommes certifiés sur SITA WorldTracer &copy;.<br />';



// ------------- CARGO SERVICE -------------
$lang["sv_cargo_title"] = "Les Services Cargo";
$lang['sv_cargo_content']= <<< EOT
    Notre équipe expérimentée et qualifiée se chargera de votre Fret dans les normes de qualité, sécurité et sureté requises. Nous fournissons les services Fret allant de la piste, passant par l'écorage, le stockage, jusqu'à la livraison au destinataire. En ce qui concerne l'exportation, nous fournissons également les services de vente, l'acceptation, la palettisation, et le chargement à l'avion. Un Service Litige Cargo est également à votre disposition.
    Une communication permanente entre nos équipes se chargeant du Cargo et les coordinateurs des vols sur la piste assure un traitement sûr et fluide de votre vol. Le tout en respectant le temps d'escale.

EOT;

// ------------- LOUNGE -------------
$lang["sv_lounge_title"] = "Les Services VIP";
$lang['sv_lounge_content']='Notre VIP lounge, muni d\'un service snack offert, permettra à vos passagers de se détendre avant le vol.';



// ------------- MEET AND ASSIST SERVICE -------------
$lang["sv_assist_title"] = "Les Services d'Assistance";
$lang['sv_assist_content']= <<< EOT
    Que ce soit à l'arrivée, au départ ou en transit, notre Service Meet-And-Assist vous fournira une équipe dédiée à votre vol pour l'ensemble de l'assistance. Cela peut inclure les passagers, l'avion, les équipages, ainsi que tous documents et autorisations. <br />
    Une agréable expérience, grâce à nos Equipes et à nos Partenaires.

EOT;

// ------------- CUSTOMIZED SERVICE -------------
$lang["sv_customize_title"] = "Les Services Personnalisés";
$lang['sv_customize_content']='...'; //todo: à définir services personnalisés


$lang['service_pride']='Nous sommes dédiés au service de nos Clients.';
$lang['service_listing']='Nous vous proposons:';

