<?php
/**
 * Created by PhpStorm.
 * User: Castiel
 * Date: 05/11/14
 * Time: 11:19
 */




class LanguageLoader {


    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
            //if()
        $site_lang = $ci->session->userdata('site_lang');
        
        if ($site_lang) {

            $ci->lang->load('navigation',$ci->session->userdata('site_lang'));
            $ci->lang->load('home',$ci->session->userdata('site_lang'));
            $ci->lang->load('aboutus',$ci->session->userdata('site_lang'));
            $ci->lang->load('customer',$ci->session->userdata('site_lang'));
            $ci->lang->load('services',$ci->session->userdata('site_lang'));
            $ci->lang->load('contactus',$ci->session->userdata('site_lang'));
            $ci->lang->load('careers',$ci->session->userdata('site_lang'));



        } else {

            $ci->lang->load('navigation','english');
            $ci->lang->load('home','english');
            $ci->lang->load('aboutus','english');
            $ci->lang->load('customer','english');
            $ci->lang->load('services','english');
            $ci->lang->load('contactus','english');
            $ci->lang->load('careers','english');


        }
    }
} 